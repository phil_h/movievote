import {inject, TestBed} from '@angular/core/testing';

import {FireAuthService} from './fire-auth.service';
import {AngularFireAuth} from '@angular/fire/auth';

fdescribe('FireAuthService', () => {
  let fAuthSpy: jasmine.SpyObj<AngularFireAuth>;
  let fireAthServ: FireAuthService;
  beforeEach(() => {
    const spy = jasmine.createSpyObj('AngularFireAuth', ['auth', 'auth.signInAnonymously']);
    TestBed.configureTestingModule({
      providers: [FireAuthService,
        {provide: AngularFireAuth, use: spy}]
    });
    fAuthSpy = spy;
    fireAthServ = new FireAuthService(spy, spy);
  });

  it('should be created', inject([FireAuthService], (service: FireAuthService) => {
    expect(service).toBeTruthy();
  }));

  it('login', () => {
    fAuthSpy.auth.signInAnonymously.apply(() => new Promise((resolve) => resolve(
      {
        'user': {'uid': 'JSzjPng3LJZKAPAs1bjl3MEbl1r2', 'displayName': 'Phil', 'photoURL': 'https://example.com/jane-q-user/profile.jpg',
          'email': null, 'emailVerified': false, 'phoneNumber': null, 'isAnonymous': true, 'providerData': [], 'apiKey':
            'AIzaSyBmckI7Focnd7VzzErNSBKCeObL1J7lNxA', 'appName': '[DEFAULT]', 'authDomain': 'movievote-2621d.firebaseapp.com',
          'stsTokenManager':
            {'apiKey': 'AIzaSyBmckI7Focnd7VzzErNSBKCeObL1J7lNxA', 'refreshToken':
                'AEXAG-f3j5DrsbvwrjKFWtTeyQs4V0SVz2dtsfTSmfk6KjbC2mCWgu9FslSmlIclEo0G74o_mfhmJJQIFl9z72fAxNZmQudMUIuNjo8MQdSUJ3hKIqx1_Y' +
                'yWzxwYrC79eF0Zzwt54EjRpfpgyNG6TzSi7uaC3fIQD1tmzdxTJ7X8p2SippkjzKz0PWkr0A7hKGNFPZmIEAY6', 'accessToken':
                'eyJhbGciOiJSUzI1NiIsImtpZCI6ImZkZjY0MWJmNDY3MTA1YzMyYWRkMDI3MGIyZTEyZDJiZTJhYmNjY2IifQ.eyJpc3MiOiJodH' +
                'RwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbW92aWV2b3RlLTI2MjFkIiwibmFtZSI6IlBoaWwiLCJwaWN0dXJlIjoiaHR0cHM6L' +
                'y9leGFtcGxlLmNvbS9qYW5lLXEtdXNlci9wcm9maWxlLmpwZyIsInByb3ZpZGVyX2lkIjoiYW5vbnltb3VzIiwiYXVkIjoibW92aWV2b3' +
                'RlLTI2MjFkIiwiYXV0aF90aW1lIjoxNTQwMjkwMDQ4LCJ1c2VyX2lkIjoiSlN6alBuZzNMSlpLQVBBczFiamwzTUVibDFyMiIsInN1YiI' +
                '6IkpTempQbmczTEpaS0FQQXMxYmpsM01FYmwxcjIiLCJpYXQiOjE1NDEyNDk4NjQsImV4cCI6MTU0MTI1MzQ2NCwiZmlyZWJhc2UiOnsiaW' +
                'RlbnRpdGllcyI6e30sInNpZ25faW5fcHJvdmlkZXIiOiJhbm9ueW1vdXMifX0.ZCICWelBzUmTJQTb-ZeHCduaLuMgcUXr4yX0QIYfa5F' +
                'dc4HOnrsFOHMKd5oGuLOgBK2HZW1df9KXMiQ87QXfMpRO_IFYssoqOMm5MY438en1LXw1IOXS-heRWmymhEz5Ir5XYJYBL0BuYg8lqNAf' +
                'vwJpzizAOIpB53mYmhVAKnnjpz3h6GwL2gOEpY_QJvtFMTKAFD3FUDYGJXzlswrSzr2vLWUBA7GMD29U5WWTLMetjx0DizZ0vcp-efK3X7V' +
                'K9_wJDhwf2oinopAn5wbJiaf7m3COQAoRXTZkGstgwBIOVNR2s4O45GpA1JItZhQLSgu_DByTuwqq8tn6m26PeA',
              'expirationTime': 1541253464635}, 'redirectEventId': null, 'lastLoginAt': '1540290048000', 'createdAt': '1540290048000'},
        'credential': null, 'additionalUserInfo': {'providerId': null, 'isNewUser': false}, 'operationType': 'signIn'})
  ));
    expect(fireAthServ.login).toBeTruthy();
  });

});
