import {Injectable} from '@angular/core';
import {MovieReference} from '../types/movieReference';
import {MVoteUser} from '../types/MVoteUser';
import {FireAuthService} from './fire-auth.service';
import {noop} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieService {
  allEventMovies: Array<MovieReference>;
  user: MVoteUser;
  constructor(private fauth: FireAuthService) {
    this.fauth.user.subscribe(user => user != null ? this.user = user : noop());
   }

  set movieList(mR: MovieReference[]) {
     this.allEventMovies = mR;
   }
  vote(selectedM: MovieReference, love: number) {
    // this.allEventMovies.forEach(movieRef => {
    //   if (movieRef.voters === undefined) {
    //   movieRef.voters = [null];
    //   }
    // });
    if (this.user == null || this.user.name === ''  || this.user.name === '?') {
      return -1; // not logged in
    }
      const newV = this.isNewVoter(selectedM.voters, this.user.uid);
    // necessary ?
    if (love === 1) {
      this.user.isLike = true;
    } else {
      this.user.isLike = false;
    }
    if (newV) {
      selectedM.votes += love;
      selectedM.voters.push(Object.assign({}, this.user));
    }
    return newV;
  }
  // voted on this movie before
  isNewVoter(voters: Array<MVoteUser>, uid: string): boolean {
    const length = voters.filter((voter) => {
      return voter != null && voter.uid === uid;
    }, uid).length;
    return length === 0;

  }
}
