import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {MVoteUser, UserInfo} from '../types/MVoteUser';
import {IndexedDBService} from './indexed-db.service';
import {Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FireAuthService {

  constructor(public afAuth: AngularFireAuth,
              private indexedDB: IndexedDBService) {
  }
  userSub = new Subject<MVoteUser>();

  changeName(name: string, user: {name: string, color: string} = null): Promise<string> {
    const currentUser = this.afAuth.auth.currentUser;
    if (currentUser.displayName == null || currentUser === undefined) {
      currentUser.updateProfile({
        displayName: name,
        photoURL: ''
      });
    }
    if (user.name != null) {
      const userc = new UserInfo(currentUser.uid, user.name, user.color);
      this.userSub.next(userc);
      return this.indexedDB.saveUserInfo(userc);
    } else {
      return new Promise((resolve, reject) => reject());
    }
  }
  changeLike(isLike: boolean) {
    const currentUser = this.afAuth.auth.currentUser;

    if (currentUser.displayName == null || currentUser === undefined) {
      currentUser.updateProfile({
        displayName: currentUser.displayName,
        photoURL: '' + isLike
      });
    }
  }

  get login() {
    return this.afAuth.auth.signInAnonymously();
  }

  get user(): Observable<MVoteUser> {
    return this.userSub.asObservable();
  }
}
