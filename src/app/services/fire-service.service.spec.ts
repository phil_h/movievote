import {TestBed} from '@angular/core/testing';

import {FireService} from './fire-service.service';
import {AngularFirestore} from '@angular/fire/firestore';

describe('FireService', () => {
  let service: FireService;
  let AngularFire: AngularFirestore;
  beforeEach(() => {
    const spy = jasmine.createSpyObj('AngularFirestore', ['.doc<Movie>.valueChanges', '.doc<Movie>.set']);
    TestBed.configureTestingModule({
      providers: [FireService,
        {provide: AngularFirestore, use: spy}]
    });
    service = TestBed.get(FireService);
    AngularFire = TestBed.get(AngularFire);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
