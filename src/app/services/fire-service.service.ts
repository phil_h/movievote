import {Injectable} from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';

import {BehaviorSubject, Observable, Subject} from 'rxjs';

import {Movie} from '../types/movie';
import {MovieReference} from '../types/movieReference';
import {MyEvent, MyEventResponse} from '../types/MyEvent';
import {take} from 'rxjs/operators';
import {FireAuthService} from './fire-auth.service';

@Injectable({
  providedIn: 'root'
})
export class FireService {
  private itemDoc: AngularFirestoreDocument<Movie>;
  private itemsCollection: AngularFirestoreCollection<MovieReference>;
  private _event: MyEvent;
  private eventNameSubject = new Subject<string>();
  eventName: string;
  private _obs = new BehaviorSubject<Observable<any[]>>(new Observable<any>());
  // user: MVoteUser;
  static getID(m: Movie) {
    return m.id;
  }

  constructor(private afs: AngularFirestore, private fauth: FireAuthService) {
    // this.fauth.user.subscribe(user => user != null ? this.user = user : noop());
  }

  getMovie(id: number, isMovie: boolean): Observable<Movie> {
    if (isMovie) {
    this.itemDoc = this.afs.doc<Movie>('/movies/' + id);
    return this.itemDoc.valueChanges();
    }
  }

  get obs() {
    return this._obs;
  }

  update(movie: MovieReference) {
    this.afs.doc<MovieReference>('/events/' + this.eventName + '/movies/' + movie.referenceId.id).update(Object.assign({}, movie));
  }

  delete(movie: MovieReference) {
    this.afs.doc<MovieReference>('/events/' + this.eventName + '/movies/' + movie.referenceId.id).delete();
  }

  addMovie(movie: Movie, isMovie) {
    const refe: MovieReference = new MovieReference(movie, isMovie);
    this.afs.doc<MovieReference>('/events/' + this.eventName + '/movies/' + FireService.getID(movie)).set(Object.assign({}, refe));
    // this.itemsCollection.add(refe);
  }

  getEventName() {
    return this.eventNameSubject.asObservable();
  }
  get event(): Observable<MyEventResponse> {
    return this.getEvent(this.eventName);
  }
  getEvent(name: string): Observable<MyEvent> {
    return this.afs.doc<MyEvent>('events/' + name).valueChanges();
  }

  changeEvent(event: string) {
    this.eventName = event;
    this.eventNameSubject.next(this.eventName);
    this.itemsCollection = this.afs.collection<any>('events/' + event +  '/movies');
    this._obs.next(this.itemsCollection.valueChanges());
  }
  getMovies() {
    this._obs.next(this.itemsCollection.valueChanges());
  }
  createEvent(event: MyEvent) {
    // second proof of new event
    this.eventName = event.name;
    if (this.eventName === '' || this.eventName === undefined || this.eventName === null) {
      return;
    }
    this.eventNameSubject.next(this.eventName);
    this.afs.doc<MyEvent>('/events/' + this.eventName).set(event);
    this.itemsCollection = this.afs.collection<any>('events/' + event.name +  '/movies');
    this._obs.next(this.itemsCollection.valueChanges());
  }

  get events(): Observable<MyEvent[]> {
    return this.afs.collection<MyEvent>('events').valueChanges();
  }

  finalBallot() {
    this.itemsCollection.valueChanges().pipe(take(1)).subscribe((movieList: MovieReference[]) => {
      this.event.pipe(take(1)).subscribe(event => {
        event.isFinalBallot = false;
        event.finalBallotwasActive = true;
        this.afs.doc<MyEvent>('/events/' + this.eventName).set(event);
      });
      movieList.sort((a, b) => b.votes - a.votes);
      const mostVotes = movieList[0].votes;
      const best = movieList.filter(movie => movie.votes === mostVotes); // number of movies with the most votes
      if (best.length < 2) {
        const secondMost = movieList[best.length].votes;
        const secondMostMovies = movieList.filter(movie => movie.votes === secondMost);
        secondMostMovies.forEach(m => best.push(m));
      }
      movieList.forEach(movie => {
        if (!best.includes(movie)) {
          this.delete(movie);
        } else {
          movie.votes = 0;
          movie.voters = [null];
          this.update(movie);
        }
      });
    });

  }
}

