import {inject, TestBed} from '@angular/core/testing';

import {MovieService} from './movie.service';
import {MovieReference} from '../types/movieReference';
import {UserInfo} from '../types/MVoteUser';
import {Movie} from '../types/movie';

describe('MovieService', () => {
  const mUser = new UserInfo('1', '?', 'green');
  let mr = new MovieReference();
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MovieService]
    });
  });

  it('should be created', inject([MovieService], (service: MovieService) => {
    expect(service).toBeTruthy();
  }));
  it('vote success', inject([MovieService], (service: MovieService) => {
    service.allEventMovies = [mr];
    expect(service.vote(mr, mUser, 1)).toBe(true);
    expect(mr.votes).toBe(2);
  }));
  it('vote fail', inject([MovieService], (service: MovieService) => {
    mr = new MovieReference(new Movie(), true, [mUser]);
    service.allEventMovies = [mr];
    expect(mr.votes).toBe(1);
    expect(service.vote(mr, mUser, 1)).toBe(false);
    expect(mr.votes).toBe(1);
  }));

  it('isNewVoter yes', inject([MovieService], (service: MovieService) => {
    expect(service.isNewVoter(mr.voters, mUser.uid)).toBe(true);
  }));
  it('isNewVoter no', inject([MovieService], (service: MovieService) => {
    mr = new MovieReference(new Movie(), true, [mUser]);
    expect(service.isNewVoter(mr.voters, mUser.uid)).toBe(false);
  }));

});
