import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {delay, map, retryWhen, take} from 'rxjs/operators';
import {Movie, MovieDetails} from '../types/movie';
import {Observable} from 'rxjs';
import {AvailableResponse} from '../types/availableResponse';
import {AvailableType} from '../types/availableType';
import {TmdbResult} from '../types/TmdbResult';
import {ReferenceId} from '../types/movieReference';

@Injectable({
  providedIn: 'root'
})
export class OmdbService {
  language = navigator.language;

  constructor(private http: HttpClient) {

  }

  search(term, resort: boolean = true): Observable<Movie[]> {
    return this.http
      .jsonp<TmdbResult[]>(`https://api.themoviedb.org/3/search/multi?api_key=2848b7850ad2fb9f93516300a225698c&query=${(term)}` +
        '&language=' + this.language, 'callback')
    .pipe(
      map(res => res['results']),
      map(movies => resort ? movies.sort((a: Movie, b: Movie) => parseFloat(b.popularity) - parseFloat(a.popularity)) : movies
      )
       )  ;
}
discover(genres: string, keywords: string, type: string): Observable<Movie[]> {
  const date = new Date();
  date.setMonth(date.getMonth() - 6);
  const dateNew = date.toJSON().slice(0, 10);

  return this.http
    .get<TmdbResult[]>(`https://api.themoviedb.org/3/discover/movie?api_key=2848b7850ad2fb9f93516300a225698c` +
      `&sort_by=popularity.desc&include_adult=false&include_video=false&page=1` +
      `&with_genres=${(genres)}&primary_release_date.lte=${(dateNew)}` + '&language=' + this.language)
    .pipe(
      map(res => res['results']),
      map(res => {
          const overviewLength = 200;
          res.forEach(movie => {
            movie.poster_path = 'https://image.tmdb.org/t/p/w185' + movie.poster_path;
            movie.overview = movie.overview.split('.')[0] + '.';
            if (movie.overview.length > overviewLength) {
              movie.overview = movie.overview.slice(0, overviewLength) + '...';
            }
          });
          return res;
        }
      ));
}

  getMovie(mReference: ReferenceId) {
    let urls = 'https://api.themoviedb.org/3/';
    if (mReference.isMovie) {
      urls += 'movie/';
    } else {
      urls += 'tv/';
    }
    urls += mReference.id + '?api_key=2848b7850ad2fb9f93516300a225698c' + '&language=' + this.language;
    return this.http.jsonp<MovieDetails>(urls, 'callback')
    .pipe(
      map(e => this.beautifyMovie(e)),
      retryWhen(err => {
        return err.pipe(delay(50), take(4));
      })
    );
  }

  beautifyMovie(e: Movie | MovieDetails): any {
    e.poster_path = 'https://image.tmdb.org/t/p/w185' + e.poster_path;
    const overviewDots = e.overview.split('.');
    if (overviewDots[0].length < 45) {
      e.overview = overviewDots[0] + '.' + overviewDots[1] + '.';
    } else {
      e.overview = e.overview.split('.')[0] + '.';
    }
    if (!e.title) {
      e.title = e.name;
      e.release_date = e.first_air_date;
      e.media_type = 'tv series';
    }
    return e;
  }
  getAvailibility(title: string) {
    const locale = navigator.languages[0].replace('-', '_'); // for browser locale specific results
    return this.http.get<AvailableResponse>
    ('https://quiet-inlet-63359.herokuapp.com/https://apis.justwatch.com/content/titles/de_DE/popular?' +
      'body={"content_types":["show","movie"],"page":1,"page_size":1,"query":"' + title.replace('&', '%26') + '"}')
    .pipe(
      map(e =>  e.items[0].offers ?
        e.items[0].offers.filter(o => o.monetization_type === 'flatrate') :
        []
        ),
      map(providers => {
        const result: AvailableType = {
          netflix: false,
          amazn: false,
          sky: false,
          hbo: false,
          hulu: false
        };
        providers.forEach(pv => {
          if (pv.urls !== undefined) {
          if (pv.urls.standard_web.includes('netflix')) {
            result.netflix = true;
          }
          if (pv.urls.standard_web.includes('amazon')) {
            result.amazn = true;
          }
          if (pv.urls.standard_web.includes('sky')) {
            result.sky = true;
          }
          if (pv.urls.standard_web.includes('hbo')) {
            result.hbo = true;
          }
            }
        });

        return result;
      })
    ).toPromise();


  }
}


