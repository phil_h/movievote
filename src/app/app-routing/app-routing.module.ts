import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from '../components/about/about.component';
import { ToolbarComponent } from '../toolbar-module/toolbar/toolbar.component';
import {ToolbarModule} from '../toolbar-module/toolbar.module';

const appRoutes: Routes = [
  { path: 'about',        component: AboutComponent },
  { path: 'new-event',    component: ToolbarComponent},
  { path: '',   redirectTo: '/events/changeMe', pathMatch: 'full' },
  { path: '**', redirectTo: '/about', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
