import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { sha3_256 } from 'js-sha3';

@Component({
  selector: 'app-password-dialog',
  templateUrl: './password-dialog.component.html',
  styleUrls: ['./password-dialog.component.css']
})
export class PasswordDialogComponent implements OnInit {
  complete = false;
  constructor(
    public dialogRef: MatDialogRef<PasswordDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {

     }
  ngOnInit() {
  }
  onNoClick(): void {
    // this.dialogRef.close(false);
  }

  onCancel() {
    this.dialogRef.close(false);
  }
  onKey(input: string) {
    if (sha3_256(input) === this.data) {
      this.dialogRef.close(true);
    }
  }

}
