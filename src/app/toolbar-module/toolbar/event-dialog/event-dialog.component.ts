import {AfterContentInit, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {MyEvent} from '../../../types/MyEvent';
import {NgForm} from '@angular/forms';
import {sha3_256} from 'js-sha3';

@Component({
  selector: 'app-event-dialog',
  templateUrl: 'event-dialog.html',
  styleUrls: ['event-dialog.css']
})
export class EventDialogComponent implements AfterContentInit {
  event = {
    name: '',
    isPrivate: false,
    voteCount: 1,
    isNew: true,
    password: '',
    isFinalBallot: false,
    date: new Date()
  };
  eventName = '';
  events;
  create = 'create';
  hint = {
    msg: 'Please enter a title',
    color: 'primary'
  };
  startDate: Date;

  constructor(
    public dialogRef: MatDialogRef<EventDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngAfterContentInit() {
    this.startDate = new Date();
    this.startDate.setDate(this.startDate.getDate() + 7);
    this.event.date = this.startDate;
  }

  onKey(event: string) { // without type info
    this.eventName = event;
    if (this.data.events.recieved === false) {
      console.log('dialog is missing events', this.data.events);

      this.data.events.observeable.subscribe((events: MyEvent[]) => {
        console.log('dialog recieved events');

        this.data.events = {
          recieved: true,
          allEvents: events
        };
      });
    }
    if (this.data.events.recieved === true) {
      this.events = this.data.events.allEvents.map(({name}) => name);
      if (this.events.includes(event)) {
        this.event.isNew = false;
        this.create = 'go to event';
        this.hint = {
          msg: 'This event is already used.',
          color: 'accent'
        };
        this.event.isPrivate = false;
      } else {
        this.event.isNew = true;
        this.create = 'create new';
        this.hint = {
          msg: 'This event title is available.',
          color: 'primary'
        };
      }
    }


  }

  onNoClick(): void {
    if (!this.data.createMode) {
      this.dialogRef.close();
    }
  }

  onSubmit(f: NgForm) {
    if (this.event.isPrivate && this.event.password !== '') {
      this.event.password = sha3_256(this.event.password);
    }
    this.dialogRef.close(this.event);
  }


}
