import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ToolbarComponent } from '../toolbar.component';
import {ToolbarModule} from '../../toolbar.module';

const eventRoutes: Routes = [
    { path: 'events/:eventName', component: ToolbarComponent }
];
@NgModule({
  imports: [
    RouterModule.forChild(eventRoutes),
    ToolbarModule
  ],
  exports: [
    RouterModule
  ]
})
export class EventRoutingModule { }
