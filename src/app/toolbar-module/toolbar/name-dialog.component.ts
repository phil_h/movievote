import {AfterContentInit, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ColorSwitchService} from '../color-picker/color-switch.service';

@Component({
  selector: 'app-name-dialog',
  templateUrl: 'name-dialog.html',
})
export class NameDialogComponent implements AfterContentInit {
  newName: string;
  name: string;
  color: string;
  constructor(
    public nameDialog: MatDialogRef<NameDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private colorS: ColorSwitchService) {
  }

  ngAfterContentInit() {
    this.nameDialog.afterOpened().subscribe(() => {
      if (this.data.name !== undefined && this.data.name !== '?') {
        this.name = this.data.name;
      }
      if (this.data.color !== 'grey') {
        setTimeout(() => this.colorS.onSelect(this.data.color), 50);
      } else {
        setTimeout(() => this.colorS.onSelect('greenyellow'), 50);
        this.color = 'greenyellow';
      }
    });
    this.colorS.selectedColorSubject.asObservable().subscribe(color => this.color = color);

  }
  onNoClick(): void {
    this.nameDialog.close();
  }


}
