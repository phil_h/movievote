import {Component, OnInit} from '@angular/core';
import {MatDialog, MatSnackBar} from '@angular/material';
import {FireService} from '../../services/fire-service.service';
import {FireAuthService} from '../../services/fire-auth.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {noop, of} from 'rxjs';
import {switchMap, take} from 'rxjs/operators';
import {PasswordDialogComponent} from './password-dialog/password-dialog.component';
import {NameDialogComponent} from './name-dialog.component';
import {EventDialogComponent} from './event-dialog/event-dialog.component';
import {MyEvent} from '../../types/MyEvent';
import {UserInfo} from '../../types/MVoteUser';
import {animate, group, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css'],
  animations: [
    trigger('expandName', [
      state('expand', style( {width: '300px', height: '400px', zIndex: '2', position: 'absolute',
        boxShadow: '0 11px 15px -7px rgba(0,0,0,.2), 0 24px 38px 3px rgba(0,0,0,.14), 0 9px 46px 8px rgba(0,0,0,.12)',
        background: '#424242', transform: 'translateY(50vh)'})),
      state('ready', style( {opacity: '1', backgroundColor: 'orange'})),
      transition('* => expand', [
        style({zIndex: '2', position: 'absolute', right: '2em', top: '1em'}),
        group([
          animate('700ms ease-in', style({width: '90vw', height: '90vh'})),
          animate('500ms ease-in', style({
            boxShadow: '0 11px 15px -7px rgba(0,0,0,.2), 0 24px 38px 3px rgba(0,0,0,.14), 0 9px 46px 8px rgba(0,0,0,.12)',
            background: '#424242'}))
        ]),
        // animate('200ms ease-in', style({opacity: '0.5'}))
      ]),
      transition('* => ready', [
        animate('500ms ease-in'),
        style( {opacity: '1', backgroundColor: 'orange'})
      ])
    ])
  ]
})
export class ToolbarComponent implements OnInit {
  title = '';
  user = new UserInfo('', '?', 'grey');
  event: MyEvent;
  isPrivate = false;
  passwordHash: string;
  voteCount = 1;
  allEvents;
  isEmptyEvemt = false;
  expanded: boolean;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog,
    private fire: FireService,
    private fAuth: FireAuthService,
    private snackBar: MatSnackBar) {
  }
  ngOnInit() {
    // this.title = this.route.snapshot.paramMap.get('eventName');
    this.fAuth.login.then(e => {
      console.log(e);
      this.fAuth.user.pipe(take(1)).subscribe(user => user ? this.user = user : noop(), err => console.error(err));
      this.route.paramMap.pipe(
        switchMap((params: ParamMap) =>
          of (params.get('eventName'))
        )
      ).subscribe((name: string) => {


        if (name === '' || name === undefined || name === null) {
          this.isEmptyEvemt = true;
          return;
        }
        this.title = name;
        this.fire.getEvent(name).subscribe((event) => {
          if (event === undefined) {
            this.isEmptyEvemt = true;
            return;
          }
          this.passwordHash = event.password;
          if (event.isPrivate) {
            this.openPasswordDialog();
            this.fire.changeEvent(name);

          } else {
            this.fire.changeEvent(name);
          }
        });
      });
      this.allEvents = {
        recieved: false,
        observeable: this.fire.events
      };
      this.allEvents.observeable.subscribe((events: MyEvent[]) => {
        this.allEvents = {
          recieved: true,
          allEvents: events
        };
        if (this.isEmptyEvemt) {
          this.openEventDialog();
        }
      });
    });
  }

  openEventDialog(): void {
    const dialogRef = this.dialog.open(EventDialogComponent, {
      width: '250px',
      position: {
        top: '3.5em'
      },
      data: {
        events: this.allEvents,
        createMode: this.isEmptyEvemt
      },
      disableClose: this.isEmptyEvemt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      if (result !== undefined && result.name) {
        this.title = result.name;
        this.isPrivate = result.isPrivate;
        if (result.isNew === true && result.name !== undefined) {
          this.fire.createEvent(result);
        } else if (result.isPrivate) {
          this.openPasswordDialog();
          this.fire.changeEvent(result.name);
        } else {
          this.snackBar.open('You changed to: ' + result.name + '.', null, {
            duration: 1200
          });
             }

        this.router.navigate(['/events/', result.name]);

      } else if (this.isEmptyEvemt) {
        this.router.navigate(['new-event']);
             }
    });

  }

  expandName() {
    this.expanded = true;
    // setTimeout(() => this.openNameDialog(), 750);
  }
  openNameDialog(): void {
    const dialogRef = this.dialog.open(NameDialogComponent, {
      width: '50%',
      minWidth: '277px;',
      position: {
        top: '3.5em',
        right: '1em'
      },
      data: this.user
    });

    dialogRef.afterClosed().subscribe(result => {
      this.expanded = false;

      if (result) {
        if (result.name.length > 1) {
          this.user.name = result.name;
        }
        this.user.color = result.color;
        this.fAuth.changeName(result.name, {name: this.user.name, color: this.user.color}).then(val => {
          this.snackBar.open('Welcome ' + this.user.name + '! 🎉', null, {
            duration: 2000
          });
        });
      }
    });

  }
  openPasswordDialog(): void {
    const dialogRef = this.dialog.open(PasswordDialogComponent, {
      width: '50%',
      data: this.passwordHash,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!result) {
        this.title = '';
        this.router.navigate(['new-event']);
      } else {
        this.snackBar.open('Passphrase correct!', null, {
          duration: 1000
        });
      }
    });

  }
  closeName() {
    this.expanded = false;
    console.log('a', this.expanded);
  }

}

