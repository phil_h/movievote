import {Component, OnInit} from '@angular/core';
import {OmdbService} from '../../services/omdb.service';
import {MatDialog} from '@angular/material';
import {FireService} from '../../services/fire-service.service';
import {FireAuthService} from '../../services/fire-auth.service';
import {MovieSearchDialogComponent} from './dialog/movie-search-dialog.component';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent implements OnInit {
  title: string;
  id: string;

  constructor(private omdb: OmdbService,
              public dialog: MatDialog,
              private fire: FireService,
              private fAuth: FireAuthService) {
  }

  ngOnInit() {
  }

  onAdd(title: string) {
    if (title) {
      this.omdb.search(title, true);
    }
  }

    openDialog(): void {
      const dialogRef = this.dialog.open(MovieSearchDialogComponent, {
         width: '25%',
        position: {
           bottom: '2em',
          right: '1em'
        }
           });
           dialogRef.afterClosed().subscribe(async result => {
             if (result) {
               let isMovie = false;
               if (result.media_type === 'movie') {
                 isMovie = true;
               }
               this.fire.addMovie(result, isMovie);
             }

           });

    }


}

