import {Component, Inject, OnInit} from '@angular/core';
import {Movie} from '../../../types/movie';
import {Observable, Subject} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {OmdbService} from '../../../services/omdb.service';
import {catchError, debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-movie-search-dialog',
  templateUrl: 'movie-search-dialog.html',
  styleUrls: ['../add-movie.component.css']

})

 export class MovieSearchDialogComponent implements OnInit {
  id: string;
  movies: Observable<Movie[]>;
  selectedMovie: Movie = new Movie();
  searchTerms = new Subject<string>();

  constructor(
    public dialogRef: MatDialogRef<MovieSearchDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private omdb: OmdbService) {

  }

  ngOnInit() {
    this.movies = this.searchTerms
      .pipe(
        debounceTime(100),        // wait 300ms after each keystroke before considering the term
        distinctUntilChanged(),   // ignore if next search term is same as previous
        // .do(_ => this.loadingIndicator = true)
        switchMap(term => term
          ? this.omdb.search(term, true)
          : new Observable<Movie[]>()),
        // .do(_ => this.loadingIndicator = false)
        catchError(error => {
          console.log(error);
          return new Observable<Movie[]>();
        }));

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onCancel() {
    this.dialogRef.close();
    // this.fire.delete(this.mRef);
  }
  onAdd(title: string) {
    this.searchTerms.next(title);
  }
  onSelect(item: Movie) {
    if (!item.title) {
      item.title = item.name;
      item.release_date = item.first_air_date;
      item.media_type = 'tv series';
    }
    item.poster_path = 'https://image.tmdb.org/t/p/w185' + item.poster_path;
    if (item.overview != null) {
      item.overview = item.overview.split('.')[0] + '.';
    }
    this.selectedMovie = item;
    setTimeout(() => this.selectedMovie.title = item.title, 30);
  }
  onEnter(event) {
    console.log('event:', event);
  }

}
