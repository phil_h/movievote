import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {FormControl} from '@angular/forms';
import {MAT_DIALOG_DATA, MatAutocompleteSelectedEvent, MatCheckboxChange, MatChipInputEvent, MatDialogRef} from '@angular/material';
import {Observable, Subject} from 'rxjs';
import {catchError, map, startWith, switchMap, take} from 'rxjs/operators';
import {OmdbService} from '../../services/omdb.service';
import {Movie} from '../../types/movie';
import {GENRES_DE, GENRES_US, GenreType} from '../../types/genres';
import {DiscoverService} from './discover.service';

@Component({
  selector: 'app-disover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.css']
})
export class DiscoverComponent implements OnInit {
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = false;
  maxMovies = 20;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  fruitCtrl = new FormControl();
  filteredFruits: Observable < string[] > ;
  fruits: Array<string> = [];
  allFruits: Array<GenreType>;
  movies: Observable < Movie[] > ;
  searchTerms = new Subject < string > ();
  genreId: string = null;
  title = 'Discover.';
  @ViewChild('fruitInput') fruitInput: ElementRef < HTMLInputElement > ;
  mCount = {
    start: 0,
    end: 7
  };
  selectedMovies: Movie[] = [];
  textfileMode = false;
  constructor(public dialogRef: MatDialogRef < DiscoverComponent > ,
              @Inject(MAT_DIALOG_DATA) public data: Array<string> | string,
              private omdb: OmdbService, private discoverService: DiscoverService) {
    if (navigator.language === 'de') {
      this.allFruits = GENRES_DE;
    } else {
      this.allFruits = GENRES_US;
    }
    this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
      startWith(null),
      // map(fruit => fruit ? fruit.name : fruit),
      map((fruit: string | null) => {
        return fruit ?
          this._filter(fruit) :
          this.allFruits.map(e => e.name)
            .filter(e => {
              let bool = true;
              this.fruits.forEach(fr => e.includes(fr) ?
                bool = false : null);
              return bool;
            })
            .slice();
      }));
  }

  ngOnInit() {
    if (this.data !== null && this.data.constructor === Array) {
      this.title = 'Import.';
      // textfile mode
      this.textfileMode = true;
      const list = this.data;
      const movieL: Movie[] = [];
      if (typeof list !== 'string') {
        this.maxMovies = 0;
        this.movies = Observable.create(ob => {
          list.forEach(line => {
            if (line !== '') {
              line = line.split('.').slice(0, -1).toString().replace(new RegExp(',', 'g'), ' '); // remove .mov
              line = line.split('(')[0];
              line = line.replace(new RegExp('_', 'g'), ' ');
              line = line.replace(' HD', '');
              line = line.replace(' HQ', '');
              line = line.replace(' en', '');
              line = line.replace(' 720p', '');

              if (line === ' ' || line === '') {
                return;
              }
              this.maxMovies++;
              this.omdb.search(line, false).pipe(take(1),
                map(movie => movie[0] ? movie[0] : new Movie())).subscribe(movie => {
                if (!movie.id) {
                  movie = new Movie();
                  movie.title = line;
                  movie.overview = 'Not Found.';
                  movie.selected = false;
                } else {
                  movie.selected = true;
                  const checkEvent = new MatCheckboxChange();
                  checkEvent.checked = true;
                  this.onAdd(movie, checkEvent);
                  this.omdb.beautifyMovie(movie);
                }
                movieL.push(movie);
                ob.next(movieL);
              });
            }
          });
        });
      }
    } else {
      this.movies = this.searchTerms
        .pipe(
          switchMap(term => term ?
            this.omdb.discover(this.genreId, term, 'movie') :
            new Observable<Movie[]>()),
          // .do(_ => this.loadingIndicator = false)
          catchError(error => {
            console.log(error);
            return new Observable<Movie[]>();
          })
        );
      setTimeout(_ => {
        if (typeof this.data === 'string') {
          this.fruits.push(this.data);
        }
        this.onInput();
      }, 5);
    }
  }
  onInput() {
    this.genreId = '';
    this.fruits.forEach(genre => {
      if (genre.includes('&')) {
        if (genre.includes('Sci-Fi')) {
          this.genreId += '878 ';
        } else {
          this.genreId += this.allFruits.filter(e => e.name === genre.split('&')[0].trim().toString()).map(e => e.id).toString() + ' ';
        }
        genre = genre.split('&')[1].trim();
      }
      this.genreId += this.allFruits.filter(e => e.name === genre.toString()).map(e => e.id).toString() + ' ';
    });
    this.genreId = this.genreId.trim().replace(new RegExp(' ', 'g'), ',');
    this.searchTerms.next(this.genreId);
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    const genre = this._filter(value);
    // Add our fruit
    if ((value || '').trim() && genre.length === 1) {
      this.fruits.push(genre[0]);
      this.onInput();
    }


    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.fruitCtrl.setValue(null);
  }

  remove(fruit: string): void {
    const index = this.fruits.indexOf(fruit);

    if (index >= 0) {
      this.fruits.splice(index, 1);
      this.onInput();
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (!this.fruits.includes(event.option.value)) {
      this.fruits.push(event.option.viewValue);
      this.onInput();
      this.mCount.end = 3;
    }
    this.fruitInput.nativeElement.value = '';
    this.fruitCtrl.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.allFruits.filter(fruit => fruit.name.toLowerCase().indexOf(filterValue) === 0).filter(e => {
        let bool = true;
        this.fruits.forEach(fr => e.name.includes(fr) ?
          bool = false : null);
        return bool;
      })
      .map(e => e.name);
  }
  showMore(event) {
    if (this.mCount.end < this.maxMovies - 1) {
      this.mCount.end += 2;
    }

  }

  onAdd(m: Movie, event: MatCheckboxChange) {
    if (event.checked) {
      this.selectedMovies.push(m);
    } else {
      const index = this.selectedMovies.indexOf(m);
      if (index > -1) {
        this.selectedMovies.splice(index, 1);
      }
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  addMovies() {
  }
}
