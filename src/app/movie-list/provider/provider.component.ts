import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OmdbService} from '../../services/omdb.service';
import {AvailableClass, AvailableType} from '../../types/availableType';
import {IndexedDBService} from '../../services/indexed-db.service';

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.css']
})
export class ProviderComponent implements OnInit {
  @Input() title: string;
  @Input() id: number;
  @Output() loaded = new EventEmitter<boolean>();
  movieAvailability: AvailableType;

  constructor(private omdb: OmdbService,
              private indexedDB: IndexedDBService) {
  }
  ngOnInit() {

    Promise.race([this.omdb.getAvailibility(this.title), this.indexedDB.getProvider(this.id)]).then(e => {
      // console.log(e);
      this.movieAvailability = e;
        this.indexedDB.saveProvider(new AvailableClass(this.id, e));
      this.loaded.emit(true);
    }, err => console.log('error ', err));
  }
}
