import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {noop, Observable, Subscription} from 'rxjs';
import {FireService} from '../../services/fire-service.service';
import {MovieReference, ReferenceId} from '../../types/movieReference';
import {MovieService} from '../../services/movie.service';
import {MatDialog} from '@angular/material';
import {FireAuthService} from '../../services/fire-auth.service';
import {debounceTime} from 'rxjs/operators';
import {MovieDetails, MovieDetailsClass} from '../../types/movie';
import {IndexedDBService} from '../../services/indexed-db.service';
import {UserData, UserDataInterface} from '../../types/MVoteUser';
import {DiscoverComponent} from '../discover/discover.component';

@Component({
  selector: 'app-list-movies',
  templateUrl: './list-movies.component.html',
  styleUrls: ['./list-movies.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ListMoviesComponent implements OnInit {
  constructor(private fire: FireService, private movieS: MovieService,
              private dialog: MatDialog, private fAuth: FireAuthService,
              private ref: ChangeDetectorRef, private indexedDB: IndexedDBService) {
  }
  movieList: MovieReference[] = [
    new MovieReference(new MovieDetailsClass(-1)),
    new MovieReference(new MovieDetailsClass(-1))];
  movieForList = new Array(2);
  items: Observable<MovieReference[]>;
  first: MovieReference = this.movieList[0];
  rowCount = Math.floor((window.innerHeight - 70) / 420) + 1;
  colCount = Math.floor((window.innerWidth / 353));

  alive = true;
  subscription: Subscription = null;
  end = 4;
  summeryList = [];
  eventName: string;



  // TODO: show loading while adding

  ngOnInit() {
    this.fire.obs.subscribe(e => {
      this.load(e);
    });
    this.fire.getEventName().subscribe(event => event ? this.eventName = event : noop());
  }

  sort() {

  }
  allowDrop(ev) {
    ev.preventDefault();
  }

  drag(ev) {
    ev.dataTransfer.setData('text', ev.target.id);
  }

  trackFn(index, item) {
    return index; // item.referenceId.id;
  }

  drop(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    // const data = ev.dataTransfer.getData('text');
    const reader = new FileReader();
    const files = ev.dataTransfer.items;
    reader.readAsText(files[0].getAsFile(), 'UTF-8');
    reader.onload = () => {
      if (reader.result instanceof ArrayBuffer) {
        return;
      }
      console.log(reader.result);
      const dialogRef = this.dialog.open(DiscoverComponent, {
        width: '92vw',
        height: '92vh',
        data: reader.result.split('\n')
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          let isMovie = false;
          result.forEach(movie => {
            console.log('movie.title.', movie.title);
            if (movie.title) {
              isMovie = true;
            }
            setTimeout(async () => {
              this.fire.addMovie(movie, isMovie);
            }, 30);
          });
        }
      });
    };

  }

  load(event) {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.subscription = event.pipe(debounceTime(150)).subscribe((e: MovieReference[]) => {
        if (Array.isArray(e)) {
          let ar: Array<MovieReference> = e;
          // first load sorting
          if (this.summeryList.length === 0 || this.movieList.length === 0) {
            Promise.all([this.indexedDB.getLikes(this.eventName), this.indexedDB.getHates(this.eventName)]).
            then((res: Array<Array<UserDataInterface>>) => {
              if (res.length === 2) {
                // this.summeryList = [];
                const likes: Array<ReferenceId> = res[0].map(data => data.mRef);
                const hates: ReferenceId[] = res[1].map(data => data.mRef);
                ar = this.sorting(ar);
                ar = this.filterSummary(ar, likes, hates);
                this.summarySorting();
                this.reloading(ar);
                this.ref.markForCheck();
              }
            });
          }
        }
    });

  }

  filterSummary(ar, likes, hates) {
    for (let j = 1; j < ar.length;) {
      let mRef = ar[j];
      for (let i = 0; i < likes.length; i) {
        if (likes[i].id === mRef.referenceId.id && likes[i].isMovie === mRef.referenceId.isMovie) {
          this.summeryList.push({movie: mRef, isLike: true});
          ar.splice(j, 1);
          mRef = ar[j];
          if (mRef == null) {
            return ar;
          }
          likes.splice(i, 1);
          continue;
        }
        i++;
      }
      for (let i = 0; i < hates.length; i) {
        if (hates[i].id === mRef.referenceId.id && hates[i].isMovie === mRef.referenceId.isMovie) {
          this.summeryList.push({movie: mRef, isLike: false});
          ar.splice(j, 1);
          hates.splice(i, 1);
          j--;
          break;
        }
        i++;
      }
      j++;
    }
    return ar;
  }

  summarySorting() {
    this.summeryList = this.summeryList.sort((a, b) => {
      if (b.isLike && !a.isLike) {
        return 1;
      }
      if (a.isLike && !b.isLike) {
        return -1;
      }
      return 0;
    });
  }

  sorting(ar: Array<MovieReference>) {
    return ar.filter(function (item, i, arg) {
      return arg.indexOf(item) === i;
    })
      .sort((n1: MovieReference, n2: MovieReference) => {
        let res = n2.votes - n1.votes;
        // sort by vote_average
        if (res === 0) {
          res = n2.vote_average - n1.vote_average;
          if (res < 0) {
            res = -1;
          }
          if (res > 0) {
            res = 1;
          }
        }
        return res;
      });
  }
  reloading(entries) {
    // check if new, dont reload if only votes change
    if (this.movieList === undefined || !(entries.length === this.movieList.length &&
      entries.every((v, i) => v.referenceId === this.movieList[i].referenceId))) {
      this.movieList = entries;
      this.movieForList = new Array(this.movieList.length);
      this.end = this.rowCount * this.colCount;
      if (this.end < 2) {
        this.end = 2;
      }
      this.ref.markForCheck();
      this.movieS.movieList = entries;
    }
  }
  showMore(event) {
    // show two more movies
    console.log('show more', this.end);
    if (this.end < this.movieList.length - this.colCount) {
      this.end += this.colCount;
    }
  }

  voted(movie: { movie: MovieDetails, isLike: boolean, mRef: MovieReference}) {
    // this.movieList.splice(0, movie)
    console.log('voted');
    const usrD = new UserData(this.eventName, {
      id: movie.movie.id,
      isMovie: !movie.movie.name
    });
    if (this.summeryList.includes(movie)) {
      return;
    }
    this.summeryList.push(movie);
    this.summarySorting();
    // remove from large list
    this.movieList.splice(this.movieList.indexOf(movie.mRef), 1);
    console.log('mlist', this.movieList);
    this.movieForList.pop();
    // if last, relaod the best
    if (this.movieList.length === 0) {
      this.movieForList = [];
      this.summeryList = [];
      this.fire.getMovies();
    }
    if (movie.isLike) {
      this.indexedDB.saveLike(usrD);
    } else {
      this.indexedDB.saveHate(usrD);
    }
    this.ref.markForCheck();
  }
  trackSummary(index, item) {
    return item.referenceId.id;
  }

}
