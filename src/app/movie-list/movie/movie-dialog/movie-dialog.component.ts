import {Component, Input, OnInit} from '@angular/core';
import {Movie} from '../../../types/movie';

@Component({
  selector: 'app-movie-dialog',
  templateUrl: './movie-dialog.component.html',
  styleUrls: ['./movie-dialog.component.css']
})
export class MovieDialogComponent implements OnInit {

  @Input() new: Movie;
  movie: Movie;
  constructor() {   }

  ngOnInit() {
    if (this.new && this.new.title) {
      this.movie = this.new;
    }
  }

}
