import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';
import {DomSanitizer, SafeStyle} from '@angular/platform-browser';

import {MovieDetails, MovieDetailsClass} from '../../types/movie';
import {OmdbService} from '../../services/omdb.service';
import {FireService} from '../../services/fire-service.service';
import {MovieReference} from '../../types/movieReference';
import {FireAuthService} from '../../services/fire-auth.service';
import {MovieService} from '../../services/movie.service';
import {AvailableType} from '../../types/availableType';
import {DiscoverComponent} from '../discover/discover.component';
import {MatDialog, MatSnackBar} from '@angular/material';
import {IndexedDBService} from '../../services/indexed-db.service';
import {animate, query, sequence, stagger, state, style, transition, trigger} from '@angular/animations';
import lozad from 'lozad';
import {ShowVotesDialogComponent} from './show-votes-dialog/show-votes-dialog.component';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    //
    trigger('movieSlideBack', [
      state('ready', style({transform: 'translateX(0)'})),
      // state('isSliding', style({transform: 'translateX(60%)'})),

      transition('* => ready', [
        animate('300ms ease-in')
      ])
    ]),
    trigger('movieSlideAwayLeft', [
      state('ready', style({transform: 'translateX(0)'})),

      // state('isSliding', style({transform: 'translateX(60%)'})),

      transition('* => gone', [
        animate('300ms ease-out', style({transform: 'translateX(-120%)', display: 'none'}))
      ]),
     // transition('gonw => ready')
    ]),
    trigger('movieSlideAwayRight', [
      state('ready', style({transform: 'translateX(0)'})),

      // state('isSliding', style({transform: 'translateX(60%)'})),

      transition('* => gone', [
        animate('400ms ease-out', style({transform: 'translateX(120%)', opacity: '0'}))
      ])
    ]),
    trigger('thumbFadeAway', [
      state('gone', style({opacity: '0', display: 'none'})),
      // state('isSliding', style({transform: 'translateX(60%)'})),

      transition('* => gone', [
        animate('300ms ease-out')
      ])
    ]),
    trigger('hateShow', [
      transition(':enter', [
        style({opacity: 0}),
        animate('5s', style({opacity: 1})),
      ]),
      transition(':increment', [
        query(':leave', [
          style({opacity: 0, width: '0px'}),
          stagger(50, [
            animate('300ms ease-out', style({opacity: 0, width: '0px'})),
          ]),
        ], {optional: true})
      ]),
      transition(':decrement', [
        animate('300ms ease-out', style({opacity: 1, width: '*', transform: 'translateX(-10px)', backgroundColor: 'red'})),
      ])
    ]),
    trigger('fadeIn', [
      transition(':enter', [
        query('.example-card, img', [
          style({opacity: 0}),
          stagger(-30, [
            animate('300ms ease-in', style({opacity: 1}))
          ])
        ])
      ]),
      transition(':leave', [
        query('.example-card', [
          style({opacity: 1}),
          stagger(-30, [
            animate('600ms ease-out', style({opacity: 0}))
          ])
        ])
      ])
    ]),
    trigger('reload', [
      transition('leave => enter', [
        style({opacity: 0.1, transform: 'translateY(16px)'}),
        animate('300ms ease-in', style({opacity: 1, transform: 'translateY(0)'}))
      ]),
      transition('* => leave', [
        style({opacity: 1}),
        sequence([
          animate('600ms ease-out', style({opacity: 0.1})),
          animate('400ms ease-in', style({opacity: 1}))
        ])
      ])
    ])
  ]
})
export class MovieComponent implements OnInit {

  @Input() set mRef(mRef: MovieReference) {
    if (mRef && mRef.referenceId && mRef.referenceId.id > 0) {
      if (!this.mRef) {
        this._mRef = mRef;
        this.reload = true;
        this.recieveMovie();
      }
      if (this.mRef.referenceId.id !== mRef.referenceId.id) {
        console.log('relaoding: ', mRef.referenceId.id, this.mRef.referenceId.id.toString());
        this._mRef = mRef;
        this.reload = true;
        setTimeout(() => this.reset(), 0);
        this.recieveMovie();
      }
      // this._mRef = mRef;
    }
  }

  get mRef() {
    return this._mRef;
  }

  constructor(
    private sanitizer: DomSanitizer, private omdb: OmdbService,
    private fire: FireService, private fAuth: FireAuthService,
    private mService: MovieService, private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private indexedDB: IndexedDBService,
    private ref: ChangeDetectorRef) {
  }
  @Output() movieVoted = new EventEmitter<{ movie: MovieDetails, isLike: boolean, mRef: MovieReference}>();
  @HostBinding('@fadeIn')
  public animatePage = true;
  _mRef;
  reload = false;
  movie: MovieDetails;
  // hasVoted: Subject<boolean> = new Subject;
  movieAvailability: AvailableType;
  loaded = false;
  x = 0;
  tX: SafeStyle;
  color: string;
  hateIcon = false;
  likeIcon = false;
  hateScale: SafeStyle;
  hateWidth = 0;
  slideState = {
    movieSlideBack: false,
    movieSlideAwayLeft: false,
    movieSlideAwayRight: false
  };

  ngOnInit() {
    const observer = lozad(); // lazy loads elements with default selector as '.lozad'
    observer.observe();
  }
  recieveMovie() {
    this.getMovie().then(e => {
        if (!e) {
          console.error('error, empty movie, retrying', e);
          // setTimeout(() => this.recieveMovie(), 300);
        } else {
          this.reload = false;
          this.movie = e;
          this.loaded = true;
          this.ref.markForCheck();
          this.indexedDB.save(new MovieDetailsClass(e.id, e));
          // this.sanitze();
          /*this.hasVoted.pipe(take(2)).subscribe(next => {
            if (next === true) {
              this.voteMSG = '';
            }
            this.hasVoted.next(!this.mService.isNewVoter(this.mRef.voters, this.fAuth.user.uid));
          });*/
        }
      },
      err => console.log('error ', err)
    );
  }
  reset() {
    setTimeout(() => {
      this.slideState.movieSlideAwayLeft = false;
      this.slideState.movieSlideAwayRight = false;
      this.hateWidth = 0;
      this.x = 0;
    this.hateIcon = false;
    this.likeIcon = false;
    this.slideState.movieSlideBack = true;
    this.color = 'grey';
    this.hateScale = this.sanitizer.bypassSecurityTrustStyle('scale(1)');
    }, 300);
  }

  private getMovie(): Promise<MovieDetails> {
    return new Promise<MovieDetails>((resolve) => {
      this.omdb.getMovie(this.mRef.referenceId).toPromise().then(movie => movie ? resolve(movie) : console.log('waiting..'));
      this.indexedDB.getMovieDetails(this.mRef.referenceId.id).then(movie => movie ?
        resolve(movie) :
        console.log('probably no data in indexeddb'));
    });
  }

  onVote(love: number): boolean {
    let msg = "";
    const voted = this.mService.vote(this.mRef, love);
    if (voted) {
      this.fire.update(this.mRef);
      return true;
    } else if (voted === -1) {
      msg = 'Please click on the circle at the top right corner to enter your nickname.';
    } else {
      msg = 'You have already voted!';
      }
      this.snackBar.open(msg, null, {
        duration: 1500
      });
    return false;
  }

  onTrailer() {
    window.open('https://www.youtube.com/results?search_query=' + this.movie.title + ' trailer', 'tab');
  }

  openDiscover(genre: string) {
    const dialogRef = this.dialog.open(DiscoverComponent, {
      width: '92vw',
      height: '92vh',
      data: genre
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        let isMovie = false;
        result.forEach(async movie => {
          console.log('movie.title.', movie.title);
          if (movie.title) {
            isMovie = true;
          }
          this.fire.addMovie(movie, isMovie);
        });
      }
    });

  }
  showVotes() {
    this.dialog.open(ShowVotesDialogComponent, {
      minWidth: '15em',
      minHeight: '300px',
      data: this.mRef.voters
    });
  }

  async like() {
    this.onVote(1);
  }

  providerLoaded() {
    this.ref.markForCheck();
  }

  hate(event) {
    this.slideState.movieSlideBack = false;
    this.slideState.movieSlideAwayRight = false;
    this.slideState.movieSlideAwayLeft = false;

    this.x = event.deltaX;
    this.tX = this.sanitizer.bypassSecurityTrustStyle('translateX(' + event.deltaX + 'px)');
    if (event.deltaX < -70) {
      this.color = 'rgba(240, 0, 0, ' + Math.abs(event.deltaX / 180) + ')';
    } else if (event.deltaX > 70) {
      this.color = 'rgba(0, 240, 0, ' + Math.abs(event.deltaX / 180) + ')';
    } else {
      this.color = 'grey';
    }
    if (Math.abs(event.deltaX) > 10) {
      if (Math.abs(event.deltaX) > 30 && Math.abs(event.deltaX) < 100) {
        this.hateScale = this.sanitizer.bypassSecurityTrustStyle('scale(' + (Math.abs(event.deltaX / 75) + 1) + ')');
      }
      this.hateWidth = 40 + Math.abs(event.deltaX * .6);
      if (event.deltaX > 0) {
        this.likeIcon = true;
      } else {
        this.hateIcon = true;
      }
    } else {
      this.hateIcon = false;
      this.likeIcon = false;
    }
  }

  finishHate(event) {
    if (Math.abs(event.deltaX) < 70) {
      this.slideState.movieSlideBack = true;
    } else if (event.deltaX < 0 && this.onVote(-1)) {
      setTimeout(() => this.movieVoted.emit({movie: this.movie, isLike: true, mRef: this.mRef}), 400 );
      this.slideState.movieSlideAwayLeft = true;
      return;
    } else if (this.onVote(1)) {
      setTimeout(() => this.movieVoted.emit({movie: this.movie, isLike: true, mRef: this.mRef}), 400 );
      this.slideState.movieSlideAwayRight = true;
      return;
    }
    this.slideState.movieSlideBack = true;
    this.hateScale = this.sanitizer.bypassSecurityTrustStyle('scale(1)');
    this.hateIcon = false;
    this.likeIcon = false;
  }
  love(event) {
    if (event.tapCount === 2) {
      console.log('love');
    }
  }
}

