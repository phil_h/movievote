
export interface MyEvent {
  name: string;
  isPrivate: boolean;
  voteCount: number;
  password: string;
  isFinalBallot: boolean;
  finalBallotwasActive: boolean;
  // @ts-ignore
  date: Date;
}

export interface MyEventResponse {
  name: string;
  isPrivate: boolean;
  voteCount: number;
  password: string;
  isFinalBallot: boolean;
  finalBallotwasActive: boolean;
  date: any;
}
