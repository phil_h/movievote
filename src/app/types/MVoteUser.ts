import {ReferenceId} from './movieReference';

export interface MVoteUser {
  uid: string;
  name: string;
  color: string;
  isLike: boolean;
}

export class UserInfo implements MVoteUser {
  constructor(public uid: string, public name: string, public color: string, public isLike: boolean = null) {
  }
}

export interface UserDataInterface {
  id: number;
  eventName: string;
  mRef: ReferenceId;
}

export class UserData implements UserDataInterface {
  id: number;

  constructor(public eventName: string, public mRef: ReferenceId) {
  }
}
