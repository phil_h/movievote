export interface MovieOMDB {
  Title: string;
  Year: number;
  Rated: string;
  Released: number;
  Runtime: string;
  Genre: string;
  Director: string;
  Writer: string;
  Actors: string;
  Plot: string;
  Language: string;
  Awards: string;
  Poster: string;
  Ratings: [{Source: string, Value: string}, {Source: string, Value: string}, {Source: string, Value: string}];
  Metascore: number;
  imdbRating: number;
  imdbVotes: number;
  imdbID: string;
  Type: string;
  DVD: string;
  BoxOffice: string;
  Production: string;
  Website: string;
  Response: boolean;
  }

export class Movie implements MovieInterface {
    id: number;
    title: string;
    vote_count: string;
  vote_average: number;
    popularity: string;
    original_language: string;
    original_title: string;
    adult: string;
    overview: string;
    release_date: string;
    poster_path: string;
    name: string;
    first_air_date: string;
    media_type: string;
  selected = false;
}

export interface MovieInterface {
  id: number;
  title: string;
  overview: string;
  release_date: string;
  poster_path: string;
  name: string;
  first_air_date: string;
  media_type: string;
  vote_average: number;

}

export interface MovieDetails extends MovieInterface {
  id: number;
  genres: {id: number, name: string}[];
  runtime: number;
  release_date: string;
  tagline: string;
  title: string;
  poster_path: string;
  overview: string;
  name: string;
  first_air_date: string;
  media_type: string;
}

export class MovieDetailsClass implements MovieDetails {
  id: number;
  genres: { id: number, name: string }[];
  runtime: number;
  release_date: string;
  tagline: string;
  title: string;
  vote_average: number;
  poster_path: string;
  overview: string;
  name: string;
  first_air_date: string;
  media_type: string;

  constructor(
    id: number,
    e?: MovieDetails,
    genres?: { id: number, name: string }[],
    runtime?: number,
    release_date?: string,
    tagline?: string,
    title?: string,
    vote_average?: number,
    poster_path?: string,
    overview?: string,
    name?: string,
    first_air_date?: string,
    media_type?: string,
  ) {
    this.id = id;
    if (e) {
      this.genres = e.genres;
      this.runtime = e.runtime;
      this.release_date = e.release_date;
      this.tagline = e.tagline;
      this.title = e.title;
      this.vote_average = e.vote_average;
      this.poster_path = e.poster_path;
      this.overview = e.overview;
      this.name = e.name;
      this.first_air_date = e.first_air_date;
      this.media_type = e.media_type;
    } else if (genres) {
      this.genres = genres;
      this.runtime = runtime;
      this.release_date = release_date;
      this.tagline = tagline;
      this.title = title;
      this.vote_average = vote_average;
      this.poster_path = poster_path;
      this.overview = overview;
      this.name = name;
      this.first_air_date = first_air_date;
      this.media_type = media_type;
    }
  }
}

