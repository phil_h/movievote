export interface AvailableType {
    netflix: boolean;
    amazn: boolean;
    sky: boolean;
    hbo: boolean;
    hulu: boolean;
}

export class AvailableClass implements AvailableType {
  id: number;
  netflix: boolean;
  amazn: boolean;
  sky: boolean;
  hbo: boolean;
  hulu: boolean;

  constructor(id: number, e: AvailableType) {
    this.id = id;
    if (e) {
      this.netflix = e.netflix;
      this.amazn = e.amazn;
      this.sky = e.sky;
      this.hbo = e.hbo;
      this.hulu = e.hulu;
    }
  }
}
