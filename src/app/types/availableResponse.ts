export interface AvailableResponse {
        items: Offers[];
        page: number;

}
export interface Offers {
    offers: {
        monetization_type: string,
        urls: {
            standard_web: string
        }
    }[];
}
