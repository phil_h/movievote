import {MVoteUser} from './MVoteUser';
import {Movie, MovieInterface} from './movie';

export class MovieReference {
  votes: number;
  referenceId: ReferenceId;
  voters: Array<MVoteUser>;
  vote_average: number;
  isAvailable: boolean;
  constructor(m: MovieInterface | MovieReference = new Movie(), isMovie = true, voters: [MVoteUser] = [null], isAvailable = false) {
    // const isMovie = m.media_type === 'movie';
    if (!(m instanceof MovieReference)) {
      this.referenceId = {id: m.id, isMovie: isMovie};
      this.votes = voters.length;
      this.voters = voters;
      this.vote_average = m.vote_average;
      this.isAvailable = false;
    } else {
      this.update(m);
    }
  }

  update(m: MovieReference) {
    this.voters = m.voters;
    this.referenceId = m.referenceId;
    this.vote_average = m.vote_average;
    this.votes = this.voters.length;
  }
  }

export interface ReferenceId {
  id: number;
  isMovie: boolean;
}

