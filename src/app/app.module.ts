import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerGestureConfig} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {OmdbService} from './services/omdb.service';

import {AppComponent} from './app.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientJsonpModule, HttpClientModule} from '@angular/common/http';

import {MatIconModule, MatNativeDateModule} from '@angular/material';

import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuth} from '@angular/fire/auth';

import {environment} from '../environments/environment';
import {FireService} from './services/fire-service.service';
import {FireAuthService} from './services/fire-auth.service';
import {MovieService} from './services/movie.service';
import {AboutComponent} from './components/about/about.component';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {EventRoutingModule} from './toolbar-module/toolbar/event-routing/event-routing.module';
import {PasswordDialogComponent} from './toolbar-module/toolbar/password-dialog/password-dialog.component';
import {MovieDialogComponent} from './movie-list/movie/movie-dialog/movie-dialog.component';
import {DiscoverComponent} from './movie-list/discover/discover.component';
import {NameDialogComponent} from './toolbar-module/toolbar/name-dialog.component';
import {EventDialogComponent} from './toolbar-module/toolbar/event-dialog/event-dialog.component';
import {MovieSearchDialogComponent} from './movie-list/add-movie/dialog/movie-search-dialog.component';
import {ServiceWorkerModule} from '@angular/service-worker';
import {MovieListModule} from './movie-list/movie-list.module';
import {ShowVotesDialogComponent} from './movie-list/movie/show-votes-dialog/show-votes-dialog.component';

export class BaluHammerConfig extends HammerGestureConfig {
  overrides = {
    pan: {
      direction: 6
    },
    pinch: {
      enable: false
    },
    rotate: {
      enable: false
    }
  };
}

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    HttpClientModule,
    HttpClientJsonpModule,

    MatIconModule,
    EventRoutingModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    MatNativeDateModule,
    MovieListModule
  ],
  providers: [
    OmdbService,
    FireService,
    AngularFireAuth,
    FireAuthService,
    MovieService,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: BaluHammerConfig
    },
  ],
  bootstrap: [AppComponent],
  entryComponents: [EventDialogComponent, NameDialogComponent, PasswordDialogComponent,
    DiscoverComponent, MovieDialogComponent, MovieSearchDialogComponent, ShowVotesDialogComponent]
})
export class AppModule { }
