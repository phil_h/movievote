// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBmckI7Focnd7VzzErNSBKCeObL1J7lNxA",
    authDomain: "movievote-2621d.firebaseapp.com",
    databaseURL: "https://movievote-2621d.firebaseio.com",
    projectId: "movievote-2621d",
    storageBucket: "movievote-2621d.appspot.com",
    messagingSenderId: "256980257883"
  }
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
